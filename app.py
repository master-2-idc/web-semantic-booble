from flask import Flask, redirect, render_template, jsonify
import requests
import spacy
from flask import request
import os
import numpy as np
import pickle
from whoosh import index
from whoosh.fields import Schema, TEXT, KEYWORD, ID, STORED
from whoosh.analysis import StemmingAnalyzer
from whoosh.query import *
from whoosh.qparser import QueryParser
from gensim.models import Word2Vec
from sklearn.metrics.pairwise import cosine_similarity
from spacy import displacy
from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop

nlp = spacy.load("fr_core_news_sm")

#opening data index
ix = index.open_dir("./static/whoosh")
model = Word2Vec.load('./static/word2vec/word2vec_model')

app = Flask(__name__, template_folder='templates')

@app.route('/', methods=["GET"])
def index():
    return render_template('index.html')

@app.route('/api/search', methods=["GET"])
def searching():
    if request.method == 'GET':
        terme = request.args.get('term')
        terme = cleanText(terme)
        terme = lemmatize(terme)[0][0]
        query = terme

        #expand query with closer words
        if terme in model.wv.vocab:
            similar_terms = model.most_similar(terme)
            for t,v in similar_terms:
                query = query+" OR "+t

        #get most similar terms and fill the query with them
        searcher = ix.searcher()

        qp = QueryParser("title", schema=ix.schema)
        q = qp.parse(u""+query)

        results = searcher.search(q, limit=100)
        #formatting the results
        responses = {}
        responses['query'] = query
        responses['array'] = []
        for res in results:
            response = {}
            response["answer"] = res["title"]
            response["question"] = res["questionIndex"]
            response["cosine"] = ""
            responses['array'].append(response)


        return jsonify({'data': responses, 'status': 200})
    return jsonify({'response': 'Access forbidden for POST request', 'status': 503})

@app.route('/api/questions', methods=["GET"])
def questions():
    if request.method == 'GET':
        termes = request.args.get('term')

        #get most similar terms and fill the query with them
        searcher = ix.searcher()

        qp = QueryParser("type", schema=ix.schema)
        q = qp.parse(u"question")

        results = searcher.search(q, limit=16)
        #formatting the results
        formatted = []
        for r in results:
            formatted.append([r["title"], r["questionIndex"]])

        return jsonify({'data': formatted, 'status': 200})
    return jsonify({'response': 'Access forbidden for POST request', 'status': 503})

@app.route('/api/sentence_search', methods=["GET"])
def sentences_search():
    sentence = request.args.get('sentence')
    query = cleanText(sentence)
    sentence_lemma = lemmatize(sentence)
    query_vector = getMeanVector(sentence_lemma)

    #load vectors model
    file = open("./static/word2vec/vectors", "rb")
    data_vector = pickle.load(file)

    res = {}
    for responseId, dataResponses in data_vector.items():
        res[responseId] = cosine_similarity([query_vector], [dataResponses['vector']])[0][0]

    sorted_res = dict(sorted(res.items(), key = lambda item : item[1], reverse= True))

    responses = {}
    responses['query'] = query
    responses['array'] = []
    for id, cosine in list(sorted_res.items())[0:100]:
        response = {}
        response["answer"] = data_vector[id]['content']
        response["question"] = data_vector[id]['title']
        response["cosine"] = str(round(cosine, 4))
        responses['array'].append(response)

    return jsonify({'data': responses, 'status': 200})


def cleanText(text):
    text = text.lower()
    text = re.sub('\n',' ', text)
    text = re.sub(' +',' ', text)
    return text

def lemmatize(text):
    sentences = []
    doc = nlp(text)
    for sent in doc.sents:
        sentence = []
        for token in sent :
            if((token.text not in fr_stop) and (not token.is_punct)):
                sentence += [token.lemma_]
        sentences += [sentence]
    return sentences

def getMeanVector(sentences_lemma):
    vectors_list = []
    for sentences in sentences_lemma:
        if sentences != []:
            for token in sentences:
                if token in model.wv.vocab:
                    vectors_list.append(model.wv[token])
                else:
                    vectors_list.append(np.random.rand(200)*(1 - (-1)) -1)
        else :
            vectors_list.append(np.ones(200)*(-1))
    return np.mean(vectors_list, axis=0)

if __name__ == "__main__":
    app.run()
