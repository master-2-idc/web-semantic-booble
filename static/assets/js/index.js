$(document).ready(async function() {
  const $search_btn  = $(".search-button")
  const $input_field = $(".main-search-input")
  const $results_ctn = $(".results-container")
  const $pagination  = $(".pagination")
  const $method_select = $(".method-select")
  const questions    = (await retrieveQuestions()).data
  const $option_button = $(".option-button")
  const $validate_options = $(".validate-options")
  const $options_container = $(".options-container")
  const $logs_container = $(".logs-container")
  const $on_button = $(".on-button")
  const $off_button = $(".off-button")

  let currentPage    = 0
  let dataCache      = []

  $search_btn.on('click', () => {
    let input_value = $input_field.val()
    let mode_value  = $method_select.val()
    let promise     = null
    switch (mode_value) {
      case 'expansion':
        promise = processRequest(input_value)
        break;
      case 'cosine':
        promise = processSentence(input_value)
        break;
      case 'named_entities':
        // todo
        break;
      default:
        console.err("No mode corresponding")
    }

    promise.then((response) => {
      addToLogPane(response.data.query)
      dataCache = response.data.array
      buildPagination(response.data.array.length)
      pageFromIndex(currentPage)
    }).catch(console.error)
  })

  $option_button.on('click', () => {
    $options_container.show()
  })

  $validate_options.on('click', () => {
    $options_container.hide()
  })

  $on_button.on('click', () => {
    $on_button.addClass('active-button')
    $on_button.removeClass('inactive-button')

    $off_button.removeClass('active-button')
    $off_button.addClass('inactive-button')

    $('article').css('width', '70%')
    $('aside').show()
  })

  $off_button.on('click', () => {
    $off_button.addClass('active-button')
    $off_button.removeClass('inactive-button')

    $on_button.removeClass('active-button')
    $on_button.addClass('inactive-button')

    $('article').css('width', '100%')
    $('aside').hide()
  })

  function buildPagination(setLength) {
    $pagination.html('')
    let totalPage = setLength/10
    for(let i = 0; i < totalPage; i++) {
      let $a = $('<a>')
      if(i == currentPage) {
        $a.attr('class', 'active')
      }
      $a.text(i)
      $a.on('click', () => {
          let index = $a.text()
          let all = $('.pagination a')
          all.each(function() {
            $(this).attr('class', '')
          })
          $a.attr('class', 'active')
          pageFromIndex(index)
      })

      $pagination.append($a)
    }
  }

  function processRequest(values) {
    return new Promise((resolve, reject) => {
      let number = values.split(' ')
      if(number.length > 1) {
          toastr.error('Ce mode ne prend en charge 1 mot uniquement', 'Erreur', {positionClass: "toast-top-right"});
          reject("String must be at most 1 word")
      }
      $.ajax({
        url: `api/search?term=${values}`,
        type: 'GET',
        dataType: 'json',
        success: function(data, status) {
          if(data.status == 200) {
            resolve(data)
          }
          reject(data)
        },
        error: function(data, status, err) {
          reject(err)
        }
      })
    })
  }

  function processSentence(sentence) {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `api/sentence_search?sentence=${sentence}`,
        type: 'GET',
        dataType: 'json',
        success: function(data, status) {
          if(data.status == 200) {
            resolve(data)
          }
          reject(data)
        },
        error: function(data, status, err) {
          reject(err)
        }
      })
    })
  }

  function buildDOMWithResponse(response) {
    $results_ctn.html("")
    response.forEach((res) => {
      let $p_question = $('<p>').attr('class', 'question')
      let $p_answer   = $('<p>').attr('class', 'answer')
      let $div        = $('<div>')

      let corresponding_question = questions.find((obj) => {
        let index = obj[1]
        return index == res.question
      })

      if(!corresponding_question) {
        corresponding_question = [res.question]
      }

      $p_question.text(corresponding_question[0])
      $p_answer.text('-'+res.answer)

      if(res.cosine) {
        $p_answer.html($p_answer.text()+`  <span class="cosine"> ${res.cosine}</span>`)
      }

      $div.append($p_question, $p_answer)

      $results_ctn.append($div)
    })
  }

  function pageFromIndex(page) {
    let index = page * 10
    let dataPart = dataCache.slice(index, index + 10)
    buildDOMWithResponse(dataPart)
  }

  function addToLogPane(query) {
    let research_term = $input_field.val()

    let date = new Date;

    let seconds = date.getSeconds();
    let minutes = date.getMinutes();
    let hour = date.getHours();

    $logs_container.append(`<h4>[${hour}:${minutes}:${seconds}] Recherche : ${research_term}</h4>`)
    $logs_container.append(`<p>${query}</p>`)
  }

  async function retrieveQuestions() {
    return new Promise((resolve, reject) => {
      $.ajax({
        url: `api/questions`,
        type: 'GET',
        dataType: 'json',
        success: function(data, status) {
          if(data.status == 200) {
            resolve(data)
          }
          reject(data)
        },
        error: function(data, status, err) {
          reject(err)
        }
      })
    })
  }
})
