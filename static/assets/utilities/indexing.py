import os, os.path
import csv
from whoosh import index
from whoosh.fields import Schema, TEXT, KEYWORD, ID, STORED, NUMERIC
from whoosh.analysis import StemmingAnalyzer
from whoosh.query import *
from whoosh.qparser import QueryParser

schema = Schema(title=TEXT(stored=True),type=TEXT(stored=True), questionIndex=NUMERIC(stored=True))

if not os.path.exists("data"):
    os.mkdir("data")

ix = index.create_in("data", schema)
ix = index.open_dir("data")
writer = ix.writer()

with open("resources/dataset.csv") as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=",")

    index = 0
    for row in csv_reader:
        if index == 0:
            #index the questions
            for i in range(10,26):
                writer.add_document(title=u""+row[i], type="question", questionIndex=i)
        else:
            for i in range(10,26):
                writer.add_document(title=u""+row[i], type="answer", questionIndex=i)
        index = index + 1


writer.commit()

all_docs = ix.searcher().documents()
for doc in all_docs:
    print(doc)

#results = searcher.search(myquery)

#print(all_docs[0]["title"])
