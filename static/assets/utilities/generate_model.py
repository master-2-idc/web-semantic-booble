import spacy
import csv
import pickle
import re
import json
import numpy as np
from spacy import displacy
from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop
from gensim.models import Word2Vec
from sklearn.metrics.pairwise import cosine_similarity
import os.path

nlp = spacy.load("fr_core_news_sm")

def transformDataset(dataset):
    res = {}
    fieldnames = dataset.fieldnames
    for row in dataset:
        for i in range(0,len(row)):
            if(i >= 11):
                questionId = i%11
                if questionId in res:
                    res[questionId]["responses"] += [row[fieldnames[i]]]
                else:
                    res[questionId] = {
                        "title" : fieldnames[i],
                        "responses" : [row[fieldnames[i]]]
                    }
    return res

def cleanText(text):
    text = text.lower()
    text = re.sub('\n',' ', text)
    text = re.sub(' +',' ', text)
    return text

def lemmatize(text):
    sentences = []
    doc = nlp(text)
    for sent in doc.sents:
        sentence = []
        for token in sent :
            if((token.text not in fr_stop) and (not token.is_punct)):
                sentence += [token.lemma_]
        sentences += [sentence]
    return sentences

def getMeanVector(sentences_lemma, word2VecModel):
    vectors_list = []
    for sentences in sentences_lemma:
        if sentences != []:
            for token in sentences:
                if token in word2VecModel.wv.vocab:
                    vectors_list.append(model.wv[token])
                else:
                    vectors_list.append(np.random.rand(200)*(1 - (-1)) -1)
        else :
            vectors_list.append(np.ones(200)*(-1))
    return np.mean(vectors_list, axis=0)

print(np.random.rand(200)*(1 - (-1)) -1)
with open("debat.csv") as csvfile:
    # Récupération / génération données pour entraîner Word2vec modèle.

    if os.path.isfile("responses_sent"):
        dataset = csv.DictReader(csvfile)
        datas = transformDataset(dataset)
        file = open("responses_sent", "rb")
        responses_sent = pickle.load(file)
    else:
        # Get all questions/responses from CSV
        dataset = csv.DictReader(csvfile)
        datas = transformDataset(dataset)
        responses = ""
        responses_sent = []
        # Lematization and cleanning of all responses
        for questionId, questionObject in datas.items():
            for response  in questionObject["responses"]:
                responses+= cleanText(response)
                responses_sent+= lemmatize(cleanText(response))
        # Dumb of a list of list that contains all sentences from responses set. (sublist = sentences = list of token)
        file = open('responses_sent','wb')
        pickle.dump(responses_sent, file)
        file.close()

    # Instanciation / récupération du word2vec modèle
    if os.path.isfile("word2vec_model"):
        model = Word2Vec.load('word2vec_model')
    else:
        model = Word2Vec(sentences=responses_sent, size=200, window=10, min_count=5, workers=2)
        model.save("word2vec_model")

    # Génération / load : réponses vectorisées stockées en Json.
    if os.path.isfile("vectors"):
        print("load")
        file = open("vectors", "rb")
        data_vector = pickle.load(file)
    else:
        print("gen")
        data_vector = {}
        responseId = 0
        for questionId, questionObject in datas.items():
            questionsTitle = questionObject['title']
            for response in questionObject['responses']:
                if response != '':
                    response_clear = cleanText(response)
                    response_lemma = lemmatize(response_clear)
                    responses_vector = getMeanVector(response_lemma, model)

                    response_data = {
                        "title" : questionsTitle,
                        "content" : response,
                        "vector" : responses_vector,
                    }
                    data_vector[responseId] = response_data
                    responseId +=1
                    print(responseId)

        file = open('vectors','wb')
        pickle.dump(data_vector, file)
